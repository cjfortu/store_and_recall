# !/bin/sh
# Title: recall.sh
# Author: Clemente Fortuna
# Date: 08/JUN/2020
# Purpose: recall tips and notes from store database file
# Updates: hw7 requirements

# variables
cwd=`pwd`
store_data="$cwd/store.db"
log_data="$cwd/activity.log"
now=`date`


if [ "$#" -eq 0 ] ; then
 more $store_data
elif [ "$1" == "-h" ]; then
 echo "Usage mode 1: './recall.sh' yields all stored notes"
 echo "Usage mode 2: './recall.sh term1 term2...' searches stored notes by search terms"
else
 # note ${PAGER:-more} allows default pager to be used unless nothing set then more used.
 grep -i "$@" $store_data | ${PAGER:-more}
fi

echo "*** Entry time & date: $now " >> $log_data
whoami >> $log_data
echo "$0" >> $log_data
echo "$input" >> $log_data
echo "$@" >> $log_data
echo "***end entry***" >> $log_data
echo "***************" >> $log_data
exit 0


