# !/bin/sh
# Title: store
# Author: Clemente Fortuna 
# Date: 08/JUN/2020
# Purpose: store tips and trips for later retrieval using recall script.
# Update: hw7 requirements.


# variables
cwd=`pwd`
store_data="$cwd/store.db"
log_data="$cwd/activity.log"
now=`date`

# start logic here
if [ "$#" -eq 0 ]; then
   echo "Enter note:"
   # note - following cat indicates input from keyboard, also this can be blank also.
   read input
   echo $input >> $store_data
   if [ "$input" == "" ]; then
      cat $store_data
      echo "store.db was printed because no note was passed"
   fi
elif [ "$1" == "-h" ]; then
   echo "Usage mode 1: './store.sh' then follow prompt"
   echo "Usage mode 2: './store.sh note1 note2...'"
   echo "user and arguments will be stored in activity.log" 
else
   echo $@ >> $store_data
fi

echo "*** Entry time & date: $now " >> $log_data
whoami >> $log_data
echo "$0" >> $log_data
echo "$input" >> $log_data
echo "$@" >> $log_data
echo "***end entry***" >> $log_data
echo "***************" >> $log_data
exit 0
